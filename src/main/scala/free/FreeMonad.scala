package free

import cats._
import cats.data.EitherK
import cats.effect.IO
import cats.implicits._

trait Log[A]
case class Info(msg: String)  extends Log[Unit]
case class Warn(msg: String)  extends Log[Unit]
case class Error(msg: String) extends Log[Unit]

trait Auth[A]
case class Login(u: String, p: String) extends Auth[Boolean]

trait Store[A]
case class Get(k: String) extends            Store[String]
case class Put(k: String, v: String) extends Store[Unit]


object AlgTypes {
  type App0[A] = EitherK[Log, Auth, A]
  type App[A]  = EitherK[App0, Store, A]
}


object LogCompiler extends (Log ~> IO) {
  def apply[A](fa: Log[A]): IO[A] = ???
}

object AuthCompiler extends (Auth ~> IO) {
  def apply[A](fa: Auth[A]): IO[A] = ???
}

object StoreCompiler extends (Store ~> IO) {
  def apply[A](fa: Store[A]): IO[A] = ???
}


object Main {
  val compiler  = LogCompiler or AuthCompiler or StoreCompiler
  val erroneous = AuthCompiler or LogCompiler or StoreCompiler
}
